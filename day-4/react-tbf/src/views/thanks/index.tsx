import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";

import { IContactFormData } from "interfaces/components/form.interfaces";
import NotFoundImage from "assets/images/oops.png";

const ThanksPageViews = () => {
  const contact = useSelector((state: any) => state.contact);

  const [formData, setFormData] = useState<IContactFormData>({
    name: "",
    email: "",
    company: "",
    message: "",
  });

  const [persistState, setPersistState] = useState<IContactFormData[]>([]);

  const persistData = () => {
    const data = JSON.parse(localStorage.getItem("persist:reactReduxPersist")!);
    if (!data) return;
    const contact = JSON.parse(data.contact);
    setPersistState(contact);
  };

  useEffect(() => {
    setFormData(
      contact[contact.length - 1] ? contact[contact.length - 1] : formData
    );
    if (contact) persistData();
  }, [contact, formData]);

  const renderTableData = () => {
    return persistState.map((contact, index) => {
      if (index === 0) return null;
      const { name, email, company, message } = contact;
      return (
        <tr key={index}>
          <td>{name}</td>
          <td>{email}</td>
          <td>{company}</td>
          <td>{message.toString().slice(0, 100)}...</td>
        </tr>
      );
    });
  };

  return (
    <StyledThanksPageViewsContainer>
      <div>
        {formData.name === "" ? (
          <>
            <h1>Something went wrong!</h1>
            <span>Go back and try again.</span>
            <img src={NotFoundImage} alt="Not Found" />
          </>
        ) : (
          <>
            <h1>Thanks for your message!</h1>
            <span>We will get back to you as soon as possible.</span>
            <div>
              <span>Here is a copy of your message:</span>
              <br />
              <span>
                Name: <p>{formData.name}</p>
              </span>
              <span>
                Email: <p>{formData.email}</p>
              </span>
              <span>
                Company: <p>{formData.company}</p>
              </span>
              <span>
                Message: <p>{formData.message.toString().slice(0, 100)}...</p>
              </span>
            </div>
          </>
        )}
      </div>

      {persistState.length > 1 && (
        <>
          <h3>Our Previous customers and their messages</h3>
          <table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Company</th>
                <th>Message</th>
              </tr>
            </thead>
            <tbody>{renderTableData()}</tbody>
          </table>
        </>
      )}
    </StyledThanksPageViewsContainer>
  );
};

export default ThanksPageViews;

const StyledThanksPageViewsContainer = styled("div")`
  display: flex;
  flex-direction: column;
  align-items: center;

  div {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    margin: 0;
    padding: 8rem 0 4rem 0;

    h1 {
      font-size: 5rem;
      text-transform: uppercase;
      font-family: ${({ theme }) => theme.font.inter};
    }
    background-color: ${({ theme }) => theme.color.greenBG};
    color: ${({ theme }) => theme.color.white};

    span {
      font-size: 1.1rem;
      color: ${({ theme }) => theme.color.grey};
    }

    img {
      width: 30%;
      background-color: ${({ theme }) => theme.color.greenBGLight};
      border-radius: 50px;
      padding: 2rem;
      margin: 2rem 0 0 0;
    }

    div {
      margin: 2rem 0 0 0;
      padding: 0;

      border-radius: 10px;
      padding: 2rem;
      width: 40%;

      background-color: ${({ theme }) => theme.color.greenBGLight};
      box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);

      span {
        width: 100%;
        display: flex;
        justify-content: space-between;
        font-size: 1rem;
        color: ${({ theme }) => theme.color.grey};
        white-space: nowrap;
        p {
          font-size: 1.2rem;
          text-align: center;
          color: ${({ theme }) => theme.color.white};
        }
      }
    }
  }

  h3 {
    font-size: 2rem;
    text-transform: uppercase;
    font-family: ${({ theme }) => theme.font.inter};
    margin: 5rem 0;
    color: ${({ theme }) => theme.color.primary};
  }

  table {
    width: 100%;
    margin: 0 0 5rem 0;

    thead {
      background-color: ${({ theme }) => theme.color.primary};
      color: ${({ theme }) => theme.color.white};
      font-size: 1.2rem;
      font-weight: 400;
      text-transform: uppercase;
      th {
        padding: 1rem;
      }
    }

    tbody {
      tr {
        background-color: ${({ theme }) => theme.color.white};
        color: ${({ theme }) => theme.color.grey};
        font-size: 1rem;
        font-weight: 300;
        td {
          padding: 1rem;
          border: 1px solid rgba(0, 0, 0, 0.2);
        }
      }
    }
  }
`;
