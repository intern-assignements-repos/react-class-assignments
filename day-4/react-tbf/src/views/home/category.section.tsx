import styled from "styled-components";

import {
  StyledAboutSection as StyledCategorySection,
  StyledAboutSectionHeader as StyledCategorySectionHeader,
  StyledLineGrey,
  StyledAboutSectionContent,
} from "styles/home/about.styles";
import { productData } from "data/productData";
import { IProduct } from "interfaces/data/product.interface";

const CategorySection = () => (
  <StyledCategorySection>
    <StyledCategorySectionHeader>
      <span>category</span>
      <StyledLineGrey />
    </StyledCategorySectionHeader>
    <StyledCategorySectionContent>
      <h2>Our Products</h2>
      <StyledProductContainer>
        {productData.map((product: IProduct) => (
          <StyledProduct key={product.id}>
            <StyledImageContainer>
              <img src={product.image} alt={product.title} />
            </StyledImageContainer>
            <h2>{product.title}</h2>
            <p>{product.description}</p>
          </StyledProduct>
        ))}
      </StyledProductContainer>
      <span>
        Additional products available with different varieties upon request
      </span>
    </StyledCategorySectionContent>
  </StyledCategorySection>
);

export default CategorySection;

export const StyledCategorySectionContent = styled(StyledAboutSectionContent)`
  width: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  span {
    margin: 20px 0;
    font-weight: 400;
    font-size: 18px;
    line-height: 27px;
    /* or 150% */

    display: flex;
    align-items: center;
    text-align: center;
  }
`;

const StyledProductContainer = styled("div")`
  display: flex;
  flex-direction: column;
  gap: 20px;
  margin-top: 50px;
`;

const StyledProduct = styled("div")`
  display: flex;

  padding: 20px 0;
  border-bottom: 2px solid rgba(0, 0, 0, 0.1);

  h2 {
    font-style: normal;
    font-weight: 500;
    font-size: 45px;
    line-height: 69px;
    /* identical to box height, or 144% */

    display: flex;
    align-items: center;
  }
`;

const StyledImageContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100px;
  height: 100px;

  border-radius: 50%;
  margin-right: 40px;
  padding: 55px;
  border: 1px solid rgba(0, 0, 0, 0.3);

  img {
    width: 70px;
  }
`;
