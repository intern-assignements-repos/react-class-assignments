import styled from "styled-components";

import { StyledLineGrey } from "styles/home/about.styles";
import RoadMap, { RoadMapLine } from "components/Map";

const RoadMapSection = () => (
  <StyledRoadMapSection>
    <RoadMapLine />
    <StyledRoadMapSectionHeader>
      <div>
        <span>Process</span>
        <StyledLineGreen />
      </div>
      <h1>Our Process</h1>
      <p>
        The oil is produced at the source using a robust and modernsolvent-free
        extraction system, designed in conjunction with Texarome's technology to
        obtain a high quality product.
      </p>
    </StyledRoadMapSectionHeader>
    <StyledRoadMapSectionContent>
      <RoadMap />
    </StyledRoadMapSectionContent>
  </StyledRoadMapSection>
);

export default RoadMapSection;

const StyledRoadMapSection = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  background: ${({ theme }) => theme.color.greenBG};
  padding: 100px 0;
  margin: 75px 0;
`;

const StyledRoadMapSectionHeader = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  /* align-items: center; */
  width: 50%;

  margin-bottom: 100px;

  div {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 35%;
    color: ${({ theme }) => theme.color.primary};

    span {
      font-style: normal;
      font-weight: 600;
      font-size: 18px;
      /* line-height: 70px; */
      /* or 389% */

      display: flex;
      align-items: center;
      text-transform: uppercase;
    }
  }

  h1 {
    font-style: normal;
    font-weight: 400;
    font-size: 60px;
    /* line-height: 79px; */
    /* or 132% */

    display: flex;
    align-items: center;

    color: ${({ theme }) => theme.color.white};
  }

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    /* line-height: 25px; */
    /* or 156% */

    display: flex;
    align-items: center;

    color: ${({ theme }) => theme.color.grey2};
  }
`;

const StyledLineGreen = styled(StyledLineGrey)`
  background: ${({ theme }) => theme.color.primary};
`;

const StyledRoadMapSectionContent = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 85%;
  z-index: 2;
`;
