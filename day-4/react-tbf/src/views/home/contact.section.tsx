import styled from "styled-components";

import {
  StyledAboutSection,
  StyledAboutSectionHeader as StyledContactSectionHeader,
  StyledLineGrey,
} from "styles/home/about.styles";
import { StyledCategorySectionContent } from "./category.section";
import { ContactForm } from "components/Form";
import { ContactInfo } from "components/Info";

const ContactSection = () => (
  <StyledContactSection id="contact">
    <StyledContactSectionHeader>
      <span
        style={{
          whiteSpace: "nowrap",
        }}
      >
        let's talk
      </span>
      <StyledLineGrey />
    </StyledContactSectionHeader>
    <StyledContactSectionContent>
      <h2>Contact Us</h2>
      <div>
        <StyledContactSectionContentLeft>
          <ContactInfo />
        </StyledContactSectionContentLeft>
        <StyledContactSectionContentRight>
          <ContactForm />
        </StyledContactSectionContentRight>
      </div>
    </StyledContactSectionContent>
  </StyledContactSection>
);

export default ContactSection;

export const StyledContactSection = styled(StyledAboutSection)`
  margin: 100px 0;
`;

const StyledContactSectionContent = styled(StyledCategorySectionContent)`
  flex-direction: column;

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
  }
`;

const StyledContactSectionContentLeft = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  ul {
    list-style: none;
    padding: 0;
    margin: 0;
    padding: 0;

    display: flex;
    flex-direction: column;

    li {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: flex-start;

      padding: 0;
      margin-bottom: 35px;

      span {
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
        /* line-height: 22px; */
        /* or 138% */

        display: flex;
        align-items: center;
        color: ${({ theme }) => theme.color.grey};

        margin: 0;
      }

      a {
        text-decoration: none;
        font-style: normal;
        font-weight: 400;
        font-size: 22px;
        /* line-height: 79px; */
        /* or 359% */

        display: flex;
        align-items: center;
        color: ${({ theme }) => theme.color.greenBG};

        margin: 0;
        padding: 0;
      }
    }
  }
`;

const StyledContactSectionContentRight = styled(
  StyledContactSectionContentLeft
)`
  width: 100%;
  margin: 0;
  padding: 0;

  form {
    width: 100%;

    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;

    input {
      width: 100%;
      height: 50px;
      border: none;
      outline: none;
      background-color: transparent;
      border-bottom: 1px solid rgba(0, 0, 0, 0.2);

      font-style: normal;
      font-weight: 400;
      font-size: 16px;

      /* or 156% */

      display: flex;
      align-items: center;

      margin: 10px 0;
      padding: 0 10px;

      &::placeholder {
        color: ${({ theme }) => theme.color.grey};
        font-family: ${({ theme }) => theme.font.lexend};
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
      }
    }

    span {
      font-family: ${({ theme }) => theme.font.inter};
      font-size: 12px;
      margin: 0;
      text-align: left;
      color: rgba(255, 0, 0, 0.5);
    }

    button {
      width: 100%;
      height: 50px;
      border: none;
      background-color: ${({ theme }) => theme.color.primary};
      color: ${({ theme }) => theme.color.white};
      font-family: ${({ theme }) => theme.font.lexend};
      font-style: normal;
      font-weight: 400;
      font-size: 15px;

      margin: 30px 0;

      cursor: pointer;
      transition: all 0.3s ease-in-out;

      &:hover {
        background-color: ${({ theme }) => theme.color.greenBG};
      }
    }
  }
`;
