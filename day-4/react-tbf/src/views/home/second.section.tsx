import React from "react";
import styled from "styled-components";

const SecondSection = () => (
  <StyledSeconSection>
    <h1>Hemp Flower Essential Oil</h1>
  </StyledSeconSection>
);

export default SecondSection;

const StyledSeconSection = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;

  color: rgba(0, 0, 0, 0.1);

  margin: 100px 0;
  h1 {
    font-family: ${({ theme }) => theme.font.crimson};
    font-style: normal;
    font-weight: 700;
    font-size: 84px;
    line-height: 112px;
    display: flex;
    align-items: center;
    text-align: center;
    letter-spacing: 8px;
    text-transform: uppercase;
  }
`;
