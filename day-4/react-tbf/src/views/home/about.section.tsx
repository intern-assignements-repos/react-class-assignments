import React from "react";

import {
  StyledAboutSection,
  StyledAboutSectionHeader,
  StyledLineGrey,
  StyledAboutSectionContent,
} from "styles/home/about.styles";

const AboutSection = () => (
  <StyledAboutSection>
    <StyledAboutSectionHeader>
      <span>about</span>
      <StyledLineGrey />
    </StyledAboutSectionHeader>
    <StyledAboutSectionContent>
      <h2>About Us</h2>
      <p>
        Combining 30 years of niche farming knowledge from the region, 10 years
        worth of genetic development, along with cutting edge industrial tools,
        our professional farming team brings artisanal quality in volumes you
        can depend on.
      </p>
    </StyledAboutSectionContent>
  </StyledAboutSection>
);

export default AboutSection;
