export interface IContactFormData {
  name: string;
  email: string;
  company: string;
  message: string;
}

export interface IContactState {
  name: string;
  email: string;
  company: string;
  message: string;
}
