export interface IThemeColor {
  primary: string;
  primaryDark: string;
  primaryLight: string;
  primaryLighter: string;

  white: string;
  black: string;
  grey: string;
  grey2: string;

  purple: string;
  blue: string;
  greenBG: string;
  greenBGLight: string;
  greenLG1: string;
}

export interface IThemeFont {
  lexend: string;
  inter: string;
  crimson: string;
  outfit: string;
}

export interface ITheme {
  color: IThemeColor;
  font: IThemeFont;
}
