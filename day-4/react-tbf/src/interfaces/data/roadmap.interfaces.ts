export interface IRoadmapElement {
  id: number;
  title: string;
  description: string;
  image: string;
}
