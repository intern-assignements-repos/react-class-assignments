import styled from "styled-components";

const StyledAboutSection = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;

  padding: 0 150px;
`;

const StyledAboutSectionHeader = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;

  span {
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 70px;
    /* or 389% */

    display: flex;
    align-items: center;
    text-transform: uppercase;
    color: ${({ theme }) => theme.color.primary};
  }
`;

const StyledLineGrey = styled("div")`
  width: 100%;
  margin: 0 10px;
  height: 2px;
  background: rgba(184, 184, 184, 0.5);
`;

const StyledAboutSectionContent = styled("div")`
  display: flex;
  margin: 20px 0;

  h2 {
    width: 100%;
    font-style: normal;
    font-weight: 400;
    font-size: 60px;
    line-height: 74px;
    /* or 123% */

    display: flex;
    align-items: center;
  }

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 30px;
    color: ${({ theme }) => theme.color.grey};
  }
`;

export {
  StyledAboutSection,
  StyledAboutSectionHeader,
  StyledLineGrey,
  StyledAboutSectionContent,
};
