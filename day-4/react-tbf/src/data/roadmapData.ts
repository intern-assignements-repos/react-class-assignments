import { IRoadmapElement } from "interfaces/data/roadmap.interfaces";

import genetics from "assets/images/svgs/rmp-1.svg";
import farming from "assets/images/svgs/rmp-2.svg";
import processing from "assets/images/svgs/rmp-3.svg";
import rectification from "assets/images/svgs/rmp-4.svg";
import rAndD from "assets/images/svgs/rmp-5.svg";
import regulatory from "assets/images/svgs/rmp-6.svg";
import qualityAssurance from "assets/images/svgs/rmp-7.svg";

export const roadmapData: IRoadmapElement[] = [
  {
    id: 1,
    title: "Genetics",
    description: `genetic hybridization, maximize terpene profiles of hempplant, minimize commonly known cannabinoids, uniquelydesigned to provide natural source of hemp EO to the F&F market. changing dna of plant to change course tosomething that has long term stability and unique application etc. `,
    image: genetics,
  },
  {
    id: 2,
    title: "Farming",
    description: `Combining 30 years of niche farming knowledge from theregion, 10 years worth of genetic development, along withcutting edge industrial tools, our professional farming team brings artisanal quality in volumes you can depend on.`,
    image: farming,
  },
  {
    id: 3,
    title: "Processing",
    description: `The oil is produced at the source using a robust and modern
    solvent-free extraction system, designed in conjunction with
    Texarome's technology to obtain a high quality product.`,
    image: processing,
  },
  {
    id: 4,
    title: "Rectification",
    description: `Low temperature molecular rectification preserves the fragile floral compounds specific to each varietal in order to remain true to the authentic aroma profile of the fresh flower.`,
    image: rectification,
  },
  {
    id: 5,
    title: `R & D`,
    description: `At the forefront of R&D, our PHD chemists and engineers use GC-MS, HPLC, headspace, and preparative chromatography in our QC lab to guide the farming, production, and product development processes.`,
    image: rAndD,
  },
  {
    id: 6,
    title: "Regulatory",
    description: `The in-house legal team with more than x years of experience, guides the regulatory department while also employing 3rd-party oversight to guarantee the absence of pesticide, heavy metal, residual solvent, and other regulated contaminants.`,
    image: regulatory,
  },
  {
    id: 7,
    title: "Quality Assurance",
    description: `The products are contained in a refrigerated vault and can
    be traced down to the farm level by batch and specific rows
    and plots, according to the certified ISO 9000 protocol.`,
    image: qualityAssurance,
  },
];
