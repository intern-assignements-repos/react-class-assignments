import { Routes, Route } from "react-router-dom";

import HomePage from "../pages/home";
import ThanksPage from "../pages/thanks";

const RouteComponent = () => (
  <Routes>
    <Route path="/" element={<HomePage />} />
    <Route path="/thanks" element={<ThanksPage />} />
  </Routes>
);

export default RouteComponent;
