import styled from "styled-components";

import { IRoadmapElement } from "interfaces/data/roadmap.interfaces";
import { roadmapData } from "data/roadmapData";
import RoadMapLine from "./mapLine.roadman";

const RoadMap = () => (
  <>
    {roadmapData.map((item: IRoadmapElement) => (
      <StyledRoadMapElement key={item.id}>
        <ImageWrapper>
          <div>
            <div>
              <img src={item.image} alt={item.title} />
            </div>
          </div>
        </ImageWrapper>
        <div>
          <h1>{item.title}</h1>
          <span>0{item.id}</span>
          <p>{item.description}</p>
        </div>
      </StyledRoadMapElement>
    ))}
  </>
);

export default RoadMap;
export { RoadMapLine };

const StyledRoadMapElement = styled("div")`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;

  width: 100%;

  margin: 70px 0;

  div {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 35%;

    position: relative;

    h1,
    span,
    p {
      align-self: flex-start;
    }

    h1 {
      font-weight: 300;
      font-size: 42px;
      line-height: 52px;

      color: rgba(255, 255, 255, 0.7);
    }

    span {
      font-weight: 300;
      font-size: 33px;

      color: ${({ theme }) => theme.color.blue};
    }

    p {
      font-weight: 400;
      font-size: 14px;
      /* line-height: 21px; */
      /* or 150% */

      display: flex;
      align-items: center;

      color: ${({ theme }) => theme.color.grey2};
    }
  }

  &:nth-child(even) {
    flex-direction: row-reverse;

    div {
      align-items: center;

      h1,
      span,
      p {
        align-self: flex-end;
        text-align: right;
      }
    }
  }
`;

const ImageWrapper = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  max-width: 187.85px;
  min-height: 157.13px;

  background: ${({ theme }) => theme.color.greenBGLight};
  box-shadow: 12px 32px 80px -20px rgba(0, 0, 0, 0.12);
  border-radius: 32px;

  margin: 0 20px;

  > div {
    display: flex;
    justify-content: center;
    align-items: center;

    box-sizing: border-box;
    /* width: 100%; */
    width: 80px;
    height: 80px;

    border: 6px solid rgba(255, 255, 255, 0.3);

    border-radius: 40px;

    div {
      width: 100%;
      height: 100%;
      border-radius: 50%;

      background: ${({ theme }) => theme.color.greenLG1};
    }
  }
`;
