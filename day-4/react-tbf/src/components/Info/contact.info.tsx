import React from "react";

const ContactInfo = () => (
  <ul>
    <li>
      <span>Email</span>
      <a href="mailto:hi@tbprocessing.com" target="_blank" rel="noreferrer">
        Hi@tbprocessing.com
      </a>
    </li>
    <li>
      <span>tel</span>
      <a href="tel:+1(855)275-3336">+1(855)275-3336</a>
    </li>
    <li>
      <span>website</span>
      <a href="https://tbprocessing.com/" target="_blank" rel="noreferrer">
        tbprocessing.com
      </a>
    </li>
  </ul>
);

export default ContactInfo;
