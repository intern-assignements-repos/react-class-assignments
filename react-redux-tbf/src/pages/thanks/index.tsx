import styled from "styled-components";

import NavBar from "components/Nav";
import ThanksPageViews from "views/thanks";
import FooterSection from "views/home/footer.section";

const ThanksPage = () => (
  <StyledThanksPage>
    <NavBar />
    <StyledSectionContainer>
      <ThanksPageViews />
      <FooterSection />
    </StyledSectionContainer>
  </StyledThanksPage>
);

export default ThanksPage;

const StyledThanksPage = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  position: relative;
`;

const StyledSectionContainer = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  min-height: 100vh;
`;
