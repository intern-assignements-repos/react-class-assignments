import styled from "styled-components";

import { LoginForm } from "components/Form";
import { FormContainer } from "views/home/contact.section";

const LoginPage = () =>  (
    <LoginFormContainer>      
      <FormContainer>
        <LoginForm />
      </FormContainer>
    </LoginFormContainer>
  );

export default LoginPage;

const LoginFormContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100vh;

  position: relative;

  & > div {
    max-width: 400px;
    border: 1px solid rgba(0, 0, 0, 0.2);
    padding: 2rem;
    border-radius: 1rem;

    background-color: ${({ theme }) => theme.color.primaryLighter};
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.3);
  }
`;
