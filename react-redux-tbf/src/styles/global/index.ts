import { createGlobalStyle } from "styled-components";
import { ITheme } from "interfaces/constants/theme.interfaces";

const GlobalStyles = createGlobalStyle<{ theme: ITheme }>`
    *, body {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        scroll-behavior: smooth;
    }

    body {
        background-color: ${({ theme }) => theme.color.white};
        font-family: ${({ theme }) => theme.font.lexend};
    }
`;

export default GlobalStyles;
