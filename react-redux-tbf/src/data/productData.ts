import SavoryImage from "assets/images/svgs/savory.svg";
import PineImage from "assets/images/svgs/pine.svg";
import SweetImage from "assets/images/svgs/sweet.svg";

import { IProduct } from "interfaces/data/product.interface";

export const productData: IProduct[] = [
  {
    id: 1,
    title: "Savory",
    image: SavoryImage,
    description: `Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.`,
  },
  {
    id: 2,
    title: "Pine",
    image: PineImage,
    description: `Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.`,
  },
  {
    id: 3,
    title: "Sweet",
    image: SweetImage,
    description: `Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt ullamco est sit aliqua.`,
  },
];
