import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import RouteComponent from "routes";
import theme from "constants/theme";
import GlobalStyles from "styles/global";

const App = () => (
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <RouteComponent />
    </ThemeProvider>
  </BrowserRouter>
);

export default App;
