import React from "react";

const FooterInfo = () => (
  <div>
    <span>Let's talk</span>
    <a href="mailto:terpenebelt@gmail.com" target="_blank" rel="noreferrer">
      terpenebelt@gmail.com
    </a>
    <a
      href="https://goo.gl/maps/9t4N45LjH33PPJmb8"
      target="_blank"
      rel="noreferrer"
    >
      430-985 Eleifend St. Duluth Washington 92611
    </a>
  </div>
);

export default FooterInfo;
