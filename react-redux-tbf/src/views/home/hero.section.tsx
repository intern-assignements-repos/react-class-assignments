import styled from "styled-components";
import HeroBgImg from "assets/images/tbf-hero-bg.png";

const HeroSection = () => (
  <StyledHeroSection>
    <StyledHeroTexts>
      <h1>Hemp Flower Essential Oil</h1>
      <p>
        Our goal in providing these products is to move into a long-term,
        sustainable market and become the leader in hemp for the Flavor &
        Fragrance industry.
      </p>
    </StyledHeroTexts>
  </StyledHeroSection>
);

export default HeroSection;

const StyledHeroSection = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  min-height: 100vh;
  position: relative;

  background-image: url(${HeroBgImg});
  background-repeat: no-repeat;
  background-size: cover;
  z-index: 1;
`;

const StyledHeroTexts = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;

  color: ${({ theme }) => theme.color.white};
  /* transform: translateY(100px); */

  h1 {
    font-weight: 400;
    font-size: 90px;
    line-height: 100px;
    /* or 111% */

    display: flex;
    align-items: center;
    text-align: center;
  }

  p {
    font-weight: 400;
    font-size: 24px;
    line-height: 40px;
    /* or 167% */
    font-family: ${({ theme }) => theme.font.inter};

    text-align: center;
    max-width: 1000px;
    color: rgba(255, 255, 255, 0.9);
  }
`;
