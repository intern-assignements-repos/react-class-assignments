import CertificateImage from "assets/images/certificate.png";
import styled from "styled-components";

const CertificateSection = () => (
  <StyledCertificateSection>
    <h1>Certificates</h1>
    <p>
      The products are contained in a refrigerated vault and canbe traced down
      to the farm level by batch and specific rows and plots, according to the
      certified ISO 9000 protocol.
    </p>
    <img src={CertificateImage} alt="Certificate" />
  </StyledCertificateSection>
);

export default CertificateSection;

const StyledCertificateSection = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  margin: 100px 0;

  h1 {
    font-style: normal;
    font-weight: 400;
    font-size: 60px;
    line-height: 79px;
    /* or 132% */

    display: flex;
    align-items: center;
    text-align: center;
  }

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 30px;
    /* or 188% */

    text-align: center;
    color: ${({ theme }) => theme.color.grey};
    width: 60%;

    margin-bottom: 50px;
  }

  img {
    width: 200px;
  }
`;
