import { createSlice } from "@reduxjs/toolkit";
import { IContactState } from "interfaces/components/form.interfaces";

const initialState: IContactState[] = [
  {
    name: "",
    email: "",
    company: "",
    message: "",
  },
];

const contactSlice = createSlice({
  name: "contact",
  initialState,
  reducers: {
    setData: (state, action) => {
      return [...state, action.payload];
    },
  },
});

export const { setData } = contactSlice.actions;

export default contactSlice.reducer;
