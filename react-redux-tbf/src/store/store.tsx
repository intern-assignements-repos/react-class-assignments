import { configureStore, combineReducers } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer, WebStorage } from "redux-persist";
import contactSlice from "./ContactSlice";

const persistConfig: { key: string; storage: WebStorage } = {
  key: "reactReduxPersist",
  storage,
};

const rootReducer = combineReducers({
  contact: contactSlice,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
});

export default store;
