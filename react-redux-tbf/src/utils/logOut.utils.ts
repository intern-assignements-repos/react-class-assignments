import isLogin from "./isLogin.utils"

const logOut = () => {
    if(isLogin()) localStorage.removeItem("tbfUser");
    window.location.reload();
}

export default logOut;