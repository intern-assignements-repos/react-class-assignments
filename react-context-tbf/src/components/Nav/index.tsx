import styled, { StyledProps } from "styled-components";

import NavLogo from "assets/images/tbf-logo.png";
import NavLogoText from "assets/images/tbf-text-logo.png";
import logOut from "utils/logOut.utils";

const NavBar = () => (
  <StyledNav>
    <StyledLogoContainer>
      <StyledNavLogo src={NavLogo} alt="TBF Logo" />
      <StyledNavLogoText src={NavLogoText} alt="TBF Text Logo" />
    </StyledLogoContainer>
    <StyledNavButton href="#contact" trans={false}>
      Contact
    </StyledNavButton>
    <StyledNavButton trans={true} onClick={() => logOut()}>
      Log Out
    </StyledNavButton>
  </StyledNav>
);

export default NavBar;

const StyledNav = styled("nav")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 20px 2rem;
  margin: 0;
  height: 5rem;
  background-color: transparent;
  width: 100%;

  position: fixed;
  z-index: 3;

  /* backdrop-filter: saturate(180%) blur(12px);
  background-color: rgba(107, 179, 162, 0.39);
  transition: all 0.8s ease-in-out 0s; */
`;

const StyledLogoContainer = styled("div")`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 2;
`;

const StyledNavLogo = styled("img")`
  height: 45px;
  width: 45px;
`;

const StyledNavLogoText = styled("img")`
  height: 45px;
  width: 200px;
`;

interface INavButtonProps {
  trans: boolean
}

const StyledNavButton = styled("a")<INavButtonProps>`
  width: 175px;
  height: 50px;
  z-index: 3;

  display: flex;
  justify-content: center;
  align-items: center;

  position: absolute;
  right: 50px;
  border-radius: 30px;
  cursor: pointer;
  text-decoration: none;
  color: ${({ theme }) => theme.color.white};
  background-color: ${({ theme }) => theme.color.primary};
  ${({trans}) => !trans && `
    transform: translateX(-110%)
  `}
`;
