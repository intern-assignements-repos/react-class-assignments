import React from "react";
import { useForm } from "react-hook-form";
import { ILoginFormData } from "interfaces/components/form.interfaces";
import saveUser from "utils/saveUser.utils";
import { useNavigate } from "react-router-dom";

const LoginForm = () => {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ILoginFormData>();

  const onSubmit = (data: ILoginFormData) => {
    saveUser(data);
    console.log(data);
    navigate("/");
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input
        type="text"
        placeholder="Enter your Email"
        {...register("email", { required: true })}
      />
      {errors?.email && <span>Email is required</span>}
      <input
        type="password"
        placeholder="Enter your Password"
        {...register("password", { required: true })}
      />
      {errors?.password && <span>Password is required</span>}
      <button type="submit">Login</button>
    </form>
  );
};

export default LoginForm;
