import React from "react";
import { useNavigate } from "react-router-dom";
import { useForm } from "react-hook-form";

import saveContact from "utils/saveContact.utils";
import ContactContext from "context/ContactContext";
import { IContactFormData } from "interfaces/components/form.interfaces";

const ContactForm = () => {
  const { setContact } = React.useContext(ContactContext);
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IContactFormData>();

  const onSubmit = (data: IContactFormData) => {
    if (data) {
      saveContact(data);
      setContact(data);
    }
    navigate("/thanks");
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input
        type="text"
        placeholder="Name"
        {...register("name", { required: true })}
      />
      {errors?.name && <span>Name is required</span>}
      <input
        type="text"
        placeholder="Your Email Address"
        {...register("email", { required: true })}
      />
      {errors?.email && <span>Email is required</span>}
      <input
        type="text"
        placeholder="Your Company Name"
        {...register("company", { required: false })}
      />
      {/* {errors?.company && <span>This field is required</span>} */}
      <input
        placeholder="Describe your needs"
        {...register("message", { required: true })}
      />
      {errors?.message && <span>Message field cannot be empty</span>}
      <button type="submit">Submit</button>
    </form>
  );
};

export default ContactForm;
