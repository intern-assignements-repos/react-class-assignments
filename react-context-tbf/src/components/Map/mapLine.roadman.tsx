import React from "react";
import styled from "styled-components";

const RoadMapLine = () => (
  <StyledRoadMapLine>
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </StyledRoadMapLine>
);

export default RoadMapLine;

const StyledRoadMapLine = styled("div")`
  display: flex;
  flex-direction: column;

  width: 100%;

  z-index: 1;

  position: relative;

  div {
    position: absolute;
  }

  div:nth-child(1) {
    top: 60px;
    left: 0;
    width: 15%;
    height: 400px;
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 50px 0 0;
  }

  div:nth-child(2) {
    top: 450px;
    left: 14.85%;
    width: 60%;
    height: 100px;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 0 50px;
  }

  div:nth-child(3) {
    top: 547.5px;
    left: 70%;
    width: 15%;
    height: 250px;
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 50px 0 0;
  }

  div:nth-child(4) {
    top: 600px;
    left: 70%;
    height: 275px;
    width: 15%;
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 50px 0;
  }

  div:nth-child(5) {
    top: 872px;
    left: 15%;
    height: 175px;
    height: 175px;
    height: 175px;
    height: 175px;
    height: 175px;
    width: 65%;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 50px 0 0 0;
  }

  div:nth-child(6) {
    top: 950px;
    left: 15%;
    height: 200px;
    width: 15%;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 0 50px;
  }

  div:nth-child(7) {
    top: 1147.5px;
    left: 30%;
    height: 200px;
    width: 55%;
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 50px 0 0;
  }

  div:nth-child(8) {
    top: 1250px;
    left: 70%;
    height: 200px;
    width: 15%;
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 50px 0;
  }

  div:nth-child(9) {
    top: 1447.5px;
    left: 15%;
    height: 200px;
    width: 55%;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 50px 0 0 0;
  }

  div:nth-child(10) {
    top: 1547.5px;
    left: 15%;
    height: 200px;
    width: 15%;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 0 50px;
  }

  div:nth-child(11) {
    top: 1745px;
    left: 30%;
    height: 200px;
    width: 55%;
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 50px 0 0;
  }

  div:nth-child(12) {
    top: 1847.5px;
    left: 70%;
    height: 200px;
    width: 15%;
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
    border-right: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 0 0 50px 0;
  }

  div:nth-child(13) {
    top: 2045px;
    left: 15%;
    height: 200px;
    width: 55%;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-top: 3px solid ${({ theme }) => theme.color.grey};
    border-radius: 50px 0 0 0;
  }

  div:nth-child(14) {
    top: 2145px;
    left: 15%;
    height: 315px;
    border-left: 3px solid ${({ theme }) => theme.color.grey};
    border-bottom: 3px solid ${({ theme }) => theme.color.grey};
  }
`;
