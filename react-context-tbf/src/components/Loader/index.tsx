import React from "react";
import styled from "styled-components";

const LoaderComponent = () => (
  <LoaderSpinner>
    <div className="lds-roller">
      <div></div>
    </div>
  </LoaderSpinner>
);

export default LoaderComponent;

const LoaderSpinner = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;

  .lds-roller {
    display: inline-block;
    position: relative;
    width: 80px;
    height: 80px;

    border: 4px solid #6d6d6d;
    border-radius: 50%;

    div {
      animation: ldsRoller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
      transform-origin: 40px 40px;
    }

    @keyframes ldsRoller {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
  }
`;
