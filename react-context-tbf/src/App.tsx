import React from "react";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import RouteComponent from "routes";
import theme from "constants/theme";
import GlobalStyles from "styles/global";
import ContactContext, { initContactState } from "context/ContactContext";
import { IContactFormData } from "interfaces/components/form.interfaces";

const App = () => {
  const [contact, setContact] =
    React.useState<IContactFormData>(initContactState);

  return (
    <ContactContext.Provider value={{ contact, setContact }}>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <GlobalStyles />
          <RouteComponent />
        </ThemeProvider>
      </BrowserRouter>
    </ContactContext.Provider>
  );
};

export default App;
