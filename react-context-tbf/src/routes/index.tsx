import { Routes as ReactRoutes, Route } from "react-router-dom";

import HomePage from "pages/home";
import ThanksPage from "pages/thanks";
import LoginPage from "pages/login";

import { IRoute } from "interfaces/routes/routes.interfaces";
import { PrivateRoutes } from "./private.routes";
import { PublicRoutes } from "./public.routes";

export const routes: IRoute[] = [
  {
    component: <HomePage />,
    path: "/",
    restricted: true,
  },
  {
    component: <ThanksPage />,
    path: "/thanks",
    restricted: true,
  },
  {
    component: <LoginPage />,
    path: "/login",
    restricted: false,
  },
];

const RouteComponent = () => (
  <ReactRoutes>
    {routes.map((route: IRoute, index: number) => {
      const { component, path, restricted } = route;

      return (
        <Route
          key={index}
          path={path}
          element={
            restricted ? (
              <PrivateRoutes component={component} />
            ) : (
              <PublicRoutes component={component} />
            )
          }
        />
      );
    })}
  </ReactRoutes>
);

export default RouteComponent;
