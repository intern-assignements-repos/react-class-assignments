import { IRoutesProps } from "interfaces/routes/routes.interfaces";
import { Navigate } from "react-router-dom";

import isLogin from "utils/isLogin.utils";


export const PrivateRoutes = ({ component }: IRoutesProps) => !isLogin() ? <Navigate to="/login" /> : component
