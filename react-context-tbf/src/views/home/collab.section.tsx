import React from "react";

import CollabImage1 from "assets/images/tbf-logo-2.png";
import CollabImage2 from "assets/images/tr-logo.png";
import styled from "styled-components";

const CollabSection = () => (
  <StyledCollabSection>
    <StyledLeftCollabSection>
      <h2>In Collaboration with</h2>
      <p>
        Amet minim mollit non deserunt ullamco est sit aliqua.Amet minim mollit
        non deserunt ullamco est sit aliqua.Amet minim mollit non deserunt
        ullamco est sit aliqua.
      </p>
    </StyledLeftCollabSection>

    <StyledRightCollabSection>
      <ImageWrapper
        style={{
          transform: "translateX(50px)",
        }}
      >
        <img src={CollabImage1} alt="CollabImage1" />
      </ImageWrapper>
      <ImageWrapper>
        <img
          src={CollabImage2}
          alt="CollabImage2"
          style={{
            width: "150px",
          }}
        />
      </ImageWrapper>
    </StyledRightCollabSection>
  </StyledCollabSection>
);

export default CollabSection;

const StyledCollabSection = styled("section")`
  display: flex;
  align-items: center;
  justify-content: center;

  background-color: ${({ theme }) => theme.color.primaryLighter};
  padding: 50px 0;
`;

const StyledLeftCollabSection = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;

  width: 40%;
  margin: 0 50px;

  h2 {
    font-style: normal;
    font-weight: 400;
    font-size: 42px;
    line-height: 79px;
    /* or 188% */

    display: flex;
    align-items: center;
  }

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 30px;
    /* or 188% */

    color: ${({ theme }) => theme.color.grey};
  }
`;

const StyledRightCollabSection = styled("div")`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  justify-items: center;
  align-items: center;
`;

const ImageWrapper = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  border: 1px solid rgba(0, 0, 0, 0.2);
  padding: 100px;

  width: 300px;
  height: 300px;
  border-radius: 50%;

  img {
    width: 200px;
  }
`;
