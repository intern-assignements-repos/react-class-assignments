import React from "react";
import styled from "styled-components";

import SliderImage1 from "assets/images/tbf-f-1.png";
import SliderImage2 from "assets/images/tbf-f-2.png";

const SliderSection = () => {
  const ref = React.useRef<HTMLDivElement>(null);

  React.useEffect(() => {
    const interval = setInterval(() => {
      ref.current?.scrollBy({ left: 1 });
    }, 50);

    return () => clearInterval(interval);
  }, []);

  return (
    <StyledSliderSection ref={ref}>
      {new Array(10).fill(0).map((_, index) => (
        <img
          key={index}
          src={index % 2 === 0 ? SliderImage1 : SliderImage2}
          alt="Slider"
        />
      ))}
    </StyledSliderSection>
  );
};

export default SliderSection;

const StyledSliderSection = styled("div")`
  display: flex;
  align-items: center;
  gap: 20px;
  margin: 100px 0;
  padding: 10px;
  margin-left: 100px;

  overflow-x: scroll;
  white-space: nowrap;
  /* transition: all 0.5s ease; */

  &::-webkit-scrollbar {
    height: 1px;
  }
  &::-webkit-scrollbar-track {
    background: #f1f1f1;
  }
  &::-webkit-scrollbar-thumb {
    background: #888;
  }
  &::-webkit-scrollbar-thumb:hover {
    background: #555;
  }

  img {
    width: 500px;
    height: 500px;
    object-fit: cover;
  }
`;
