import React from "react";
import styled from "styled-components";

import { FooterInfo } from "components/Info/";

import FooterImage from "assets/images/tbf-logo-3.png";

const FooterSection = () => (
  <StyledFooter>
    <StyledFooterTop>
      <FooterInfo />
      <div>
        <span>Explore</span>
        <a href="#">Contact</a>
        <a href="#">Privacy</a>
      </div>
      <img src={FooterImage} alt="footer logo" />
    </StyledFooterTop>
    <StyledFooterBottom>
      <div>
        <span>© 2023 terpenebeltprocessing. All Rights Reserved.</span>
      </div>
      <div>
        <span>Cookies Policy</span>
        <span>Privacy Policy</span>
        <span>Terms & Conditions</span>
      </div>
    </StyledFooterBottom>
  </StyledFooter>
);

export default FooterSection;

const StyledFooter = styled("section")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 0;
  padding-top: 50px;

  background-color: ${({ theme }) => theme.color.primaryLighter};
`;

const StyledFooterTop = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 100px;
  padding-bottom: 30px;

  border-bottom: 1px solid rgba(0, 0, 0, 0.15);

  > div {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: flex-start;

    span {
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      /* line-height: 22px; */
      /* or 138% */

      display: flex;
      align-items: center;
      color: ${({ theme }) => theme.color.grey};

      margin-bottom: 20px;
    }

    a {
      text-decoration: none;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      /* line-height: 79px; */
      /* or 494% */

      display: flex;
      align-items: center;

      color: ${({ theme }) => theme.color.greenBG};

      margin-bottom: 20px;
    }
  }

  img {
    width: 310px;
    height: 58.03px;
  }
`;

const StyledFooterBottom = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 100px;
  margin: 15px 0;

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
      font-style: normal;
      font-weight: 400;
      font-size: 15px;
      /* line-height: 22px; */
      /* or 183% */

      display: flex;
      align-items: center;

      margin: 0 20px;
    }
  }
`;
