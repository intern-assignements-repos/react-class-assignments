import React from "react";

import HeroSection from "./hero.section";
import SecondSection from "./second.section";
import AboutSection from "./about.section";
import SliderSection from "./slider.section";
import CategorySection from "./category.section";
import RoadMapSection from "./roadmap.section";
import CertificateSection from "./certificate.section";
import CollabSection from "./collab.section";
import ContactSection from "./contact.section";
import FooterSection from "./footer.section";

const HomeViews = () => (
  <>
    <HeroSection />
    <SecondSection />
    <AboutSection />
    <SliderSection />
    <CategorySection />
    <RoadMapSection />
    <CertificateSection />
    <CollabSection />
    <ContactSection />
    <FooterSection />
  </>
);

export default HomeViews;
