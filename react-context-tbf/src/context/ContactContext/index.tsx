import { IContactFormData } from "interfaces/components/form.interfaces";
import { createContext } from "react";

export const initContactState: IContactFormData = {
    name: "",
    email: "",
    company: "",
    message: ""
}

export interface IDefaultValue {
    contact: IContactFormData
    setContact: (contact: IContactFormData) => void
}

export default createContext<IDefaultValue>({
    contact: initContactState,
    setContact: function (contact: IContactFormData) {
        this.contact = contact;
    }
});
