
const getContact = () => {
    const contactList = localStorage.getItem("contactContext")
    if(contactList) return JSON.parse(contactList);
    return null;
}

export default getContact;