import { IContactFormData } from "interfaces/components/form.interfaces";
import getContact from "./getContact.utils";

const saveContact = (contact: IContactFormData) => {
    if(contact) {
        const previousData = getContact();
        if(previousData){
            const newData = [...previousData, contact];
            localStorage.setItem("contactContext", JSON.stringify(newData));
            return;
        }        
        localStorage.setItem("contactContext", JSON.stringify([contact]));
    }
}

export default saveContact;