import { IUser } from "interfaces/context/context.interfaces";

const saveUser = (user: IUser) => {
    if(user) localStorage.setItem("tbfUser", JSON.stringify(user));
}

export default saveUser;