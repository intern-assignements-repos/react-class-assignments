import { ReactComponentElement } from "react";

export interface IRoute {
  component: ReactComponentElement<any>;
  path: string;
  restricted: boolean;
}

export interface IRoutesProps {
  component: ReactComponentElement<any>;
}
