import { ITheme } from "interfaces/constants/theme.interfaces";

const theme: ITheme = {
  color: {
    primary: "#A0CE6D",
    primaryDark: "#727D35",
    primaryLight: "#81C83D",
    primaryLighter: "#ECF4ED",

    white: "#F5F5F5",
    black: "#000000",
    grey: "#6F8587",
    grey2: "#A6B0B2",

    purple: "#9D5296",
    blue: " #15C4C6",
    greenBG: "#2C4548",
    greenBGLight: "#39575A",
    greenLG1: `linear-gradient(180deg, #678648 0%, #375148 100%)`,
  },
  font: {
    lexend: `'Lexend Deca', sans-serif`,
    inter: `'Inter', sans-serif`,
    crimson: `'Crimson Text', serif`,
    outfit: `'Outfit', sans-serif`,
  },
};

export default theme;
