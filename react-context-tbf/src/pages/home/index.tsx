import styled from "styled-components";

import NavBar from "components/Nav";
import HomeViews from "views/home";

const HomePage = () => (
  <StyledHome>
    <NavBar />
    <StyledSectionContainer>
      <HomeViews />
    </StyledSectionContainer>
  </StyledHome>
);

export default HomePage;

const StyledHome = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  position: relative;
`;

const StyledSectionContainer = styled("div")`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
