import React from "react";

import FormComponent from "../../components/form";
import { LoginPageContainer } from "../../styles/pages/login/login.styles";
import { IInputData } from "../../interfaces/login.interfaces";

const LoginPage = () => {

  const onSubmitHandler = (e: React.FormEvent) => {
    e.preventDefault();

    const firstName: string = (
      document.getElementById("firstName") as HTMLInputElement
    ).value;
    const lastName: string = (
      document.getElementById("lastName") as HTMLInputElement
    ).value;
    const email: string = (document.getElementById("email") as HTMLInputElement)
      .value;
    const phone: string = (document.getElementById("phone") as HTMLInputElement)
      .value;
    const address: string = (
      document.getElementById("addr") as HTMLTextAreaElement
    ).value;
    const age: number = parseInt(
      (document.getElementById("age") as HTMLInputElement).value
    );
    const gender: string = (document.getElementById("age") as HTMLInputElement)
      .value;

    const inputData: IInputData = {
      firstName,
      lastName,
      email,
      phone,
      age,
      gender,
      address,
    };

    console.log("Data :", inputData);
  };

  return (
    <LoginPageContainer>
      <FormComponent onSubmitHandler={onSubmitHandler} />
    </LoginPageContainer>
  );
};

export default LoginPage;
