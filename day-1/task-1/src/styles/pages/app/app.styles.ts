import styled from "styled-components";

export default styled.div`
    width: 100%;
    min-height: 100vh;
    margin: 0;
    padding: 0;
`;