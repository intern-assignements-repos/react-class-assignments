import {createGlobalStyle} from "styled-components";

export default createGlobalStyle`
    body {
        margin: 0;
        padding: 0;        
        font-family: Open-Sans, Helvetica, Sans-Serif;
        background-color: #f0f0f0;
    }

    .root {
        width: 100%;
        height: 100%;
    }
`;