import { IFormField } from "../../interfaces/login.interfaces";

const FormElement = (props: { formItem: IFormField }) => {
  const { formItem } = props;

  const renderInput = (item: IFormField) => {
    switch (item.inputType) {
      case "select":
        return (
          <select id={item.id}>
            {item.options?.map((optionItem) => {
              return (
                <option key={optionItem.option} value={optionItem.value}>{optionItem.option}</option>
              );
            })}
          </select>
        );
      default:
        return (
          <input
            id={item.id}
            type={item.inputType}
            placeholder={item.placeholder}
          />
        );
    }
  };

  return (
    <>
      <label htmlFor={formItem.id}>{formItem.label}</label>
      {renderInput(formItem)}
    </>
  );
};

export default FormElement;
