import React from "react";
import LoginBGImage from "../../assets/images/bg-1.jpeg";
import { formFieldData } from "../../data/formFieldData";
import { IFormField, IFromCompProps } from "../../interfaces/login.interfaces";
import FormElement from "./FormElement";

const FormComponent = (props: IFromCompProps) => {
  const { onSubmitHandler } = props;

  return (
    <form
      action="#"
      method=""
      onSubmit={(e: React.FormEvent) => onSubmitHandler(e)}
    >
      <img src={LoginBGImage} alt="login-bg" />

      <div>
        {formFieldData.map((formItem: IFormField) => {
          return (
            <fieldset key={formItem.id}>
              <FormElement formItem={formItem} />
            </fieldset>
          );
        })}

        <button type="submit" id="submit-btn">
          Submit
        </button>
      </div>
    </form>
  );
};

export default FormComponent;
