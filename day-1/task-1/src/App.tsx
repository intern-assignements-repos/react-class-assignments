import React from 'react';
import LoginPage from './pages/login';
import AppContainer from './styles/pages/app/app.styles'

function App() {
  return (
    <AppContainer>
      <LoginPage />    
    </AppContainer>
  );
}

export default App;
