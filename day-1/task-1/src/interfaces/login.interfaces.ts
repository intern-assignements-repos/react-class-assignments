export interface IInputData {
  firstName: string;
  lastName: string;
  email: string;
  age?: number;
  gender?: string;
  phone: string;
  address: string;
}

export interface IFromCompProps {
  onSubmitHandler: {
    (e: React.FormEvent): void;
  };
}

export interface IFormField {
  id: string;
  label: string;
  inputType: string;
  placeholder?: string;
  options?: {
    value: string;
    option: string;
  }[];
}

export type IFormFieldData = IFormField[];
