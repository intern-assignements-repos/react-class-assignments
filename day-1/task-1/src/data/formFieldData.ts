import { IFormFieldData } from "../interfaces/login.interfaces";

export const formFieldData: IFormFieldData = [
    {
        id: "firstName",
        label: "First Name",
        inputType: "text",
        placeholder: "Enter your First Name"        
    },
    {
        id: "lastName",
        label: "Last Name",
        inputType: "text",
        placeholder: "Enter your Last Name"        
    },
    {
        id: "email",
        label: "Email",
        inputType: "email",
        placeholder: "Enter your Email"        
    },
    {
        id: "gender",
        label: "Gender",
        inputType: "select",
        options: [
            {
               value: "other" ,
               option: "Select"
            },
            {
               value: "male" ,
               option: "Male"
            },
            {
               value: "female" ,
               option: "Female"
            },
            {
               value: "other" ,
               option: "Other"
            },
        ]       
    },
    {
        id: "age",
        label: "Age",
        inputType: "number",
        placeholder: "Enter your Age"        
    },
    {
        id: "phone",
        label: "Phone",
        inputType: "number",
        placeholder: "Enter your Phone No."        
    },
    {
        id: "address",
        label: "Address",
        inputType: "textarea",
        placeholder: "Enter your Address"        
    }
]