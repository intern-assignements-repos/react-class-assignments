import { IData } from "interfaces/components/forms/forms.interfaces";

const validUser: IData = {
  email: "test@email.com",
  password: "password",
};

const checkValidation = (user: IData) =>
  validUser.email === user.email && validUser.password === user.password
    ? true
    : false;

const useAuth = () => checkValidation;

export default useAuth;
