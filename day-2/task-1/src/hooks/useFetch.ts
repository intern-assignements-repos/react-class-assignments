import { ITodo } from "interfaces/pages/dashboard.interface";
import { useState, useEffect } from "react";

const useFetch = (url: string) => {
  const [data, setData] = useState<ITodo[]>([]);

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => setData(data));
  }, [url]);

  return data;
};

export default useFetch;
