export interface IData {
  email: string;
  password: string;
}

export interface IFormComponentProps {
  onFormSubmit: (data: IData) => void
}