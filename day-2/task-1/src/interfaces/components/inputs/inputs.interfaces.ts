export interface IInputComponentProps {
    type: string
    id: string
    placeholder: string
    register: any
  }