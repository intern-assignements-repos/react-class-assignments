import { Outlet } from "react-router-dom";

import { AppContainer } from "styles/app/app.styles";

const App = () => (
  <AppContainer>
    <Outlet />
  </AppContainer>
);

export default App;
