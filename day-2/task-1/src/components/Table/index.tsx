import { ITodo } from "interfaces/pages/dashboard.interface";
import { StyledTable } from "styles/components/Table/table.styles";

const TableComponent = (props: { data: ITodo[] }) => {
  const { data } = props;

  return (
    <StyledTable>
      <thead>
        <tr>
          <th>Sl No.</th>
          <th>User Id</th>
          <th>Task Name</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody></tbody>
      {data?.map((item) => (
        <tr key={item.id}>
          <td>#{item.id}</td>
          <td>{item.userId}</td>
          <td>
            <span>{item.title}</span>
          </td>
          <td
            style={{
              backgroundColor: item.completed
                ? "rgba(0,255, 0, 0.5)"
                : "#ffe6007f",
            }}
          >
            {item.completed ? "Completed" : "In-Progress"}
          </td>
        </tr>
      ))}
    </StyledTable>
  );
};

export default TableComponent;
