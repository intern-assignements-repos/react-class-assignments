import { IInputComponentProps } from "interfaces/components/inputs/inputs.interfaces";

const InputComponent = (props: IInputComponentProps) => {
  const { type, id, placeholder, register } = props;

  return (
    <>
      <label htmlFor={id}>{id.toUpperCase()}</label>
      <input type={type} id={id} placeholder={placeholder} {...register(id, {required: "Field is Required"})} />
    </>
  );
};

export default InputComponent;
