import { useForm } from "react-hook-form";

import LoginBGImage from "assets/images/loginBg.jpeg";
import { StyledForm, StyledFormImage } from "styles/page/loginPage.styles";
import {
  IData,
  IFormComponentProps,
} from "interfaces/components/forms/forms.interfaces";
import InputComponent from "components/Input";

const FormComponent = (props: IFormComponentProps) => {
  const { onFormSubmit } = props;

  const {
    register,
    handleSubmit,
    formState: { errors: error },
  } = useForm<IData>();

  return (
    <StyledForm onSubmit={handleSubmit((data: IData) => onFormSubmit(data))}>
      <StyledFormImage src={LoginBGImage} alt="login-bg" />

      <div>
        <fieldset>
          <InputComponent
            type="text"
            id="email"
            register={register}
            placeholder="Enter your Email"
          />
          <div style={{
              color: "red"
            }}>{error?.email?.message}</div>

          <InputComponent
            type="password"
            id="password"
            register={register}
            placeholder="Enter your Password"
            />
            <div style={{
              color: "red"
            }}>{error?.password?.message}</div>
        </fieldset>

        <button type="submit" id="submit-btn">
          Submit
        </button>
      </div>
    </StyledForm>
  );
};

export default FormComponent;
