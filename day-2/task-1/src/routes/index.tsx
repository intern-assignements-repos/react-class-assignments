import { RouterProvider, createBrowserRouter } from "react-router-dom";

import App from "../App";
import Dashboard from "pages/dashboard";
import LoginPage from "pages/login";

const OurRoutes = () => {
  const routes = createBrowserRouter([
    {
      path: "/",
      element: <App />,
      children: [
        {
          path: "/",
          element: <LoginPage />,
        },
        {
          path: "dashboard",
          element: <Dashboard />,
        },
      ],
    },
  ]);

  return <RouterProvider router={routes} />;
};

export default OurRoutes;
