import React from "react";
import { LoginPageContainer } from "styles/page/loginPage.styles";
import FormComponent from "components/Form";
import { IData } from "interfaces/components/forms/forms.interfaces";
import { useNavigate } from "react-router-dom";
import useAuth from "hooks/useAuth";

const LoginPage = () => {
  const navigate = useNavigate();
  const checkValidation = useAuth();

  const onFormSubmit = (data: IData): void => {
    const valid = checkValidation(data);
    valid ? navigate("dashboard") : alert("Wrong Credentials");
    console.log(data);
  };

  return (
    <LoginPageContainer>
      <FormComponent onFormSubmit={onFormSubmit} />
    </LoginPageContainer>
  );
};

export default LoginPage;
