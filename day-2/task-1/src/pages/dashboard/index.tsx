import TableComponent from "components/Table";
import useFetch from "hooks/useFetch";

const api = "https://jsonplaceholder.typicode.com/todos";

const Dashboard = () => {
  const fetchedData = useFetch(api);

  return (
    <>
      <h1>Dashboard</h1>
      <TableComponent data={fetchedData} />
    </>
  );
};

export default Dashboard;
