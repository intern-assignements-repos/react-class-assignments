import styled from "styled-components";

export const LoginPageContainer = styled("div")`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  min-height: 100vh;
  background-color: #e0e0e0;

  form {
    display: flex;
    justify-content: space-between;

    background: #e0e0e0;
    box-shadow: 20px 20px 60px #bebebe, -20px -20px 60px #ffffff;

    margin: 2rem;
    padding: 2rem;
    border-radius: 2rem;
    max-width: 80vw;

    position: relative;

    img {
      width: 400px;
      border-radius: 2rem;
      object-fit: cover;
    }

    div {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      align-items: center;
      min-width: 30vw;
      list-style: none;
      fieldset {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100%;
        border: none;

        label {
          width: 60%;
          font-size: 1rem;
          letter-spacing: 1px;
          color: #bebebe;
          text-align: left;

          margin: 1rem;
        }

        input,
        textarea,
        select {
          border: none;
          font-size: 1rem;
          padding: 0.8rem 1rem;
          outline: none;
          font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
            Oxygen, Ubuntu, Cantarell, "Open Sans", "Helvetica Neue", sans-serif;
          font-weight: 300;
          width: 60%;

          border-radius: 2rem;
          background: #e0e0e0;
          box-shadow: inset 20px 20px 60px #bebebe,
            inset -20px -20px 60px #ffffff;
        }

        select {
          width: 64%;
          box-shadow: 20px 20px 60px #bebebe, -20px -20px 60px #ffffff;
        }
      }

      button {
        border: none;
        outline: none;
        font-size: 1.5rem;
        margin: 2rem;
        padding: 0.5rem 4rem;
        border-radius: 2rem;
        cursor: pointer;
        transition: all 0.5s ease-in-out;
        background: #e0e0e0;
        box-shadow: 20px 20px 60px #bebebe, -20px -20px 60px #ffffff;

        &:hover {
          padding: 0.5rem 6rem;
        }
      }
    }
  }
`;

export const StyledForm = styled("form")`
  display: flex;
  justify-content: space-between;

  background: #e0e0e0;
  box-shadow: 20px 20px 60px #bebebe, -20px -20px 60px #ffffff;

  margin: 2rem;
  padding: 2rem;
  border-radius: 2rem;
  max-width: 80vw;

  position: relative;
`;

export const StyledFormImage = styled("img")`
  width: 400px;
  border-radius: 2rem;
  object-fit: cover;
`;
