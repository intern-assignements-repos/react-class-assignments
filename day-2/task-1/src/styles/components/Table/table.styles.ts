import styled from "styled-components";

export const StyledTable = styled("table")`
  min-width: 70%;
  margin: 1em;

  tr,
  td,
  th {
    margin: 0;
    border: 1px solid whitesmoke;
    padding: 1em;
  }
`;
