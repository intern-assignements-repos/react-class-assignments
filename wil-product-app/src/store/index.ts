import { configureStore, createSlice, combineReducers } from "@reduxjs/toolkit";

import { IUser, IProduct } from "interfaces";
import { persistReducer } from "redux-persist";

const initialState: IUser = {
  id: 0,
  email: "",
  username: "",
  password: "",
  name: {
    firstname: "",
    lastname: "",
  },
  address: {
    geolocation: {
      lat: "",
      long: "",
    },
    city: "",
    street: "",
    number: 0,
    zipcode: "",
  },
  phone: "",
  __v: 0,
};

const userReducer = createSlice({
  name: "user",
  initialState,
  reducers: {
    setCurrentUser: (state, action) => {
      state = action.payload;
      return state;
    },
  },
});

const initialStateProducts: IProduct[] = [];

const productsReducer = createSlice({
  name: "products",
  initialState: initialStateProducts,
  reducers: {
    setProducts: (state, action) => {
      state = action.payload;
      return state;
    },
  },
});

const initialStateModal: boolean = false;

const modalReducer = createSlice({
  name: "modal",
  initialState: initialStateModal,
  reducers: {
    setModal: (state, action) => {
      state = action.payload;
      return state;
    },
  },
});

const persistConfig: { key: string; storage: Storage } = {
  key: "wpaReduxRoot",
  storage: localStorage,
};

const rootReducer = combineReducers({
  user: userReducer.reducer,
  products: productsReducer.reducer,
  modal: modalReducer.reducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
});

export const { setCurrentUser } = userReducer.actions;
export const { setProducts } = productsReducer.actions;
export const { setModal } = modalReducer.actions;

export default store;
