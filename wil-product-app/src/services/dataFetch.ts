import axios from "axios";
import { IUser, IProduct } from "interfaces";

export const userFetch = async (url: string) => {
  const {
    data,
  }: {
    data: IUser[] | undefined;
  } = await axios.get(url);
  return data;
};

export const productFetch = async (url: string) => {
  const {
    data,
  }: {
    data: IProduct[] | undefined;
  } = await axios.get(url);
  return data;
};
