import { ReactComponentElement } from "react";

interface IUserAddress {
  geolocation: {
    lat: string;
    long: string;
  };
  city: string;
  street: string;
  number: number;
  zipcode: string;
}

export interface IUser {
  id: number;
  email: string;
  username: string;
  password: string;
  name: {
    firstname: string;
    lastname: string;
  };
  address: IUserAddress;
  phone: string;
  __v: number;
}

export interface IProduct {
  id: number;
  title: string;
  price: number;
  description: string;
  category?: string;
  image?: string;
  rating?: {
    rate?: number;
    count?: number;
  };
}

export interface ILoginCred {
  email: string;
  password: string;
}

export interface ITheme {
  colors: {
    primary: string;
    primaryLight: string;
    secondary: string;

    background: string;
    backgroundDark: string;
    backgroundLight: string;

    white: string;
    black: string;
    blackLight: string;
    grey: string;
    fade: string;
  };
  fonts: {
    inter: string;
    poppins: string;
  };
}

export interface IRoute {
  component: ReactComponentElement<any>;
  path: string;
  restricted: boolean;
  children?: IRoute[];
}

export interface IRouteComponentProps {
  component: ReactComponentElement<any>;
  children?: IRouteComponentProps[];
}
