import styled from "styled-components";

import { ITheme } from "interfaces";
import GlobalStyles from "./global.styles";

export { GlobalStyles };

export const StyledLoginPage = styled("div")<{ theme: ITheme }>`
  width: 100%;
  min-height: 100vh;
  position: relative;
  overflow: hidden;
`;

export const StyledBGCircles = styled("div")<{ theme: ITheme }>`
  width: inherit;
  height: inherit;

  z-index: 1;
  div {
    position: absolute;

    left: 26.32%;
    right: 51.81%;
    top: 20.97%;
    bottom: 57.17%;

    width: 300px;
    height: 300px;

    background-color: ${({ theme }) => theme.colors.primary};
    border-radius: 50%;

    &:nth-child(even) {
      left: 54.24%;
      right: 23.89%;
      top: 57.17%;
      bottom: 7.51%;
    }
  }
`;

export const StyledBGSquare = styled("div")<{ theme: ITheme }>`
  width: inherit;
  height: inherit;

  * {
    z-index: 1;
  }

  div {
    position: absolute;
    left: 72.36%;
    right: -20.97%;
    /* top: -56.28%; */
    bottom: 77.8%;

    width: 700px;
    height: 700px;

    background-color: ${({ theme }) => theme.colors.primary};
    border-radius: 170px;

    &:nth-child(even) {
      left: -13.54%;
      right: 74.86%;
      top: 76.12%;
      bottom: -38.57%;
    }
  }
`;

export const StyledLoginPageContent = styled("div")<{ theme: ITheme }>`
  width: 100%;
  height: 100vh;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  * {
    z-index: 3;
  }
`;

export const LoginFormContainer = styled("div")<{ theme: ITheme }>`
  background-color: ${({ theme }) => theme.colors.white};
  min-height: 566px;
  width: 550px;
  border-radius: 10px;

  display: flex;
  flex-direction: column;
  align-items: center;

  padding: 50px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.25);
`;

export const ImageWrapper = styled("div")<{ theme: ITheme }>`
  width: 100%;
  height: 100px;

  img {
    width: 150px;
  }
`;

export const LoginFormHeader = styled("div")<{ theme: ITheme }>`
  width: 100%;

  display: flex;
  flex-direction: column;

  margin: 30px 0;

  p {
    margin-top: 10px;
    font-family: ${({ theme }) => theme.fonts.poppins};
    font-style: normal;
    font-weight: 300;
    font-size: 16px;

    color: ${({ theme }) => theme.colors.blackLight};

    a {
      color: ${({ theme }) => theme.colors.secondary};
      font-weight: 600;
    }
  }
`;

export const StyledLoginForm = styled("form")<{ theme: ITheme }>`
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;

  .form-group {
    display: flex;
    flex-direction: column;

    width: 100%;
    border: 2px solid ${({ theme }) => theme.colors.grey};
    border-radius: 1rem;
    padding: 0 1rem;
    margin-bottom: 2rem;

    position: relative;

    input,
    textarea {
      border: none;

      font-family: ${({ theme }) => theme.fonts.poppins};
      font-style: normal;
      font-weight: 300;
      font-size: 16px;
      line-height: 24px;
      /* identical to box height */

      color: ${({ theme }) => theme.colors.blackLight};

      padding: 20px;
      width: 100%;

      outline: none;

      &::placeholder {
        color: rgba(39, 39, 39, 0.4);
      }
    }

    img {
      width: 25px;
      position: absolute;
      right: 1rem;
      top: 1.2rem;
      color: ${({ theme }) => theme.colors.secondary};

      cursor: pointer;
    }

    .error {
      position: absolute;
      bottom: -1.5rem;
      color: rgba(255, 0, 0, 0.8);
    }
  }

  .forget-password {
    border: none;
    background: none;
    text-align: right;
    a {
      color: ${({ theme }) => theme.colors.secondary};
      font-weight: 500;
      font-size: 16px;
      line-height: 21px;
      text-decoration: none;
      font-family: ${({ theme }) => theme.fonts.poppins};

      transition: all 0.3s ease-in-out;
      &:hover {
        text-decoration: underline;
      }
    }
  }
`;

export const StyledButton = styled("button")<{ theme: ITheme }>`
  border: none;
  background-color: ${({ theme }) => theme.colors.secondary};
  color: ${({ theme }) => theme.colors.white};

  font-family: ${({ theme }) => theme.fonts.poppins};
  font-style: normal;
  font-weight: 600;
  font-size: 18px;

  padding: 20px;
  border-radius: 1rem;
  cursor: pointer;

  transition: all 0.3s ease-in-out;

  &:hover {
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
  }
`;

export const StyledDashboardPage = styled("div")`
  display: flex;
  justify-content: space-between;

  width: 100%;
  height: 100vh;
  background-color: ${({ theme }: { theme: ITheme }) =>
    theme.colors.backgroundLight};
`;

export const StyledDashboardContent = styled("div")`
  width: 100%;
  min-height: 100vh;

  display: flex;
  flex-direction: column;
  align-items: center;

  gap: 50px;
`;

export const StyledDashboardHeader = styled("div")`
  display: flex;
  justify-content: flex-end;
  width: 100%;

  background-color: ${({ theme }: { theme: ITheme }) =>
    theme.colors.backgroundDark};

  padding: 15px 30px;

  div {
    cursor: pointer;
    border-radius: 50%;

    display: flex;
    align-items: center;

    transition: all 0.3s ease-in-out;
    &:hover {
      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
    }
  }
`;

export const StyledPrimaryButton = styled(StyledButton)`
  width: 145px;
  font-weight: 700;
  font-size: 14px;
  border-radius: 20px;
  padding: 12px 20px;
`;

export const StyledDashboardBody = styled("div")<{ theme: ITheme }>`
  display: flex;
  flex-direction: column;

  width: 95%;
  min-height: 80vh;
  padding: 2rem;

  box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.1);
  border-radius: 20px;
  background-color: ${({ theme }: { theme: ITheme }) => theme.colors.white};

  > div {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;

    > h1 {
      font-weight: 600;
      font-size: 24px;
    }

    &:nth-child(2) {
      justify-content: center;
      align-items: center;
    }
  }

  table {
    width: 100%;
    min-height: 65vh;
    border-collapse: collapse;
    margin: 1rem 0 0 0;

    thead {
      border-radius: 20px;

      tr {
        background-color: ${({ theme }: { theme: ITheme }) =>
          theme.colors.grey};
        border-radius: 20px;

        text-align: left;

        th {
          padding: 1rem 0;
          font-weight: 600;
          font-size: 16px;

          min-width: 100px;
          /* border-radius: 20px; */

          &:nth-child(1) {
            border-radius: 10px 0 0 10px;
            width: 100px;
            text-align: center;
          }

          &:nth-child(3) {
            min-width: 200px;
          }

          &:nth-child(4) {
            border-radius: 0 10px 10px 0;
          }
        }
      }
    }

    tbody {
      tr {
        border-bottom: 2px solid
          ${({ theme }: { theme: ITheme }) => theme.colors.grey};

        td {
          padding: 1rem 0;
          font-weight: 300;
          font-size: 16px;

          /* text-align: center; */

          &:nth-child(1) {
            text-align: center;
          }
        }
      }
    }

    tfoot {
      tr {
        td {
          padding: 1rem 0;
          font-weight: 300;
          font-size: 16px;

          text-align: center;

          button {
            width: 40px;
            height: 40px;

            border-radius: 50%;
            margin: 0 0.5rem;

            border: none;
            outline: none;
            background: none;
            cursor: pointer;
            transition: all 0.3s ease-in-out;

            background-color: ${({ theme }: { theme: ITheme }) =>
              theme.colors.secondary};
            color: ${({ theme }: { theme: ITheme }) => theme.colors.white};

            &:hover {
              box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.5);
            }

            &.disabled {
              background-color: #c4c4c4;
              color: ${({ theme }: { theme: ITheme }) => theme.colors.white};
              cursor: not-allowed;
            }
          }
        }
      }
    }
  }
`;

export const StyledInput = styled("input")<{ theme: ITheme }>``;
