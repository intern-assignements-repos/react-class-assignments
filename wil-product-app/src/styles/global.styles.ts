import { ITheme } from "interfaces";
import { createGlobalStyle } from "styled-components";

export default createGlobalStyle<{ theme: ITheme }>`
    *, body, html {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: ${({ theme }) => theme.fonts.inter};
        color: ${({ theme }) => theme.colors.blackLight};
    }

    body {
        background-color: ${({ theme }) => theme.colors.background};
        min-height: 100vh;
        position: relative;
        /* overflow: hidden; */
    }
`;
