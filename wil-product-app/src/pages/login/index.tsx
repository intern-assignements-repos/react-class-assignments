import React from 'react'
import { StyledLoginPage } from 'styles'
import LoginPageViews from 'views/login'

const LoginPage = () => {
  return (
    <StyledLoginPage>
      <LoginPageViews />
    </StyledLoginPage>
  )
}

export default LoginPage
