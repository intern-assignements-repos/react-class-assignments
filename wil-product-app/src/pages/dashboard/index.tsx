import styled from "styled-components";

import {
  StyledDashboardPage,
  StyledDashboardContent,
  StyledDashboardHeader,
} from "styles";
import NavBar from "component/Nav";
import { logOut } from "utils/logOut.utils";
import PowerBtnSvg from "assets/svgs/power.svg";
import ProductPage from "./product.page";
import UserPage from "./user.page";
import { useState } from "react";

const DashboardPage = () => {
  const [page, setPage] = useState("product");

  return (
    <StyledDashboardPage>
      <NavBar page={page} setPage={(page: string) => setPage(page)} />
      <StyledDashboardContent>
        <StyledDashboardHeader>
          <div onClick={() => logOut()}>
            <img src={PowerBtnSvg} alt="power" />
          </div>
        </StyledDashboardHeader>
        {page === "product" ? <ProductPage /> : <UserPage />}
      </StyledDashboardContent>
    </StyledDashboardPage>
  );
};

export default DashboardPage;
