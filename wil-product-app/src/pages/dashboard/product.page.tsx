import React from "react";
import { useSelector, useDispatch } from "react-redux";

import TableComponent from "component/Table";
import ButtonComponent from "component/Button";
import { StyledDashboardBody } from "styles";
import { setModal } from "store";
import AddProductModal from "component/Modal/AddProduct.modal";

const ProductPage = () => {
  const modal = useSelector((state: any) => state.modal);
  const dispatch = useDispatch();

  const handleAddProduct = () => {
    dispatch(setModal(!modal));
  };

  return (
    <StyledDashboardBody>
      <div>
        <h1>Products</h1>
        <ButtonComponent
          btnType="primary"
          content="Add Product"
          onClick={handleAddProduct}
        />
      </div>
      <TableComponent option="products" />
      <AddProductModal />
    </StyledDashboardBody>
  );
};

export default ProductPage;
