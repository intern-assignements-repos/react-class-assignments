import React from "react";

import TableComponent from "component/Table";
import { StyledDashboardBody } from "styles";
import ButtonComponent from "component/Button";

const UserPage = () => {
  return (
    <StyledDashboardBody>
      <div>
        <h1>Users</h1>
        <ButtonComponent btnType="primary" content="Add User" />
      </div>
      <TableComponent option="users" />
    </StyledDashboardBody>
  );
};

export default UserPage;
