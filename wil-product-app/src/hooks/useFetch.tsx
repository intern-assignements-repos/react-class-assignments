import React from "react";
import axios from "axios";

import { IUser, IProduct } from "interfaces";
import { isProducts, getProducts, setProducts } from "utils/products.utils";
import { isUsers, getAllUsers, setUsers } from "utils/users.utils";

const forProduct = async (api: string) => {
  if (isProducts()) {
    return getProducts();
  }

  const result = await axios(api);
  setProducts(result.data as IProduct[]);
  return result.data as IProduct[];
};

const forUser = async (api: string) => {
  if (isUsers()) {
    return getAllUsers();
  }

  const result = await axios(api);
  setUsers(result.data as IUser[]);
  return result.data as IUser[];
};

const useFetch = (url: string, options?: "users" | "products") => {
  const [data, setData] = React.useState<
    IUser[] | IProduct[] | undefined | null
  >(null);
  const [loading, setLoading] = React.useState(true);

  const fetchData = async () => {
    if (options === "users") {
      const result = await forUser(url);
      setData(result);
    } else {
      const result = await forProduct(url);
      setData(result);
    }

    setLoading(false);
  };

  React.useEffect(() => {
    fetchData();
  }, [url, options]);

  return { data, loading };
};

export default useFetch;
