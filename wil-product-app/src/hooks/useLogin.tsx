import { useState } from "react";
import { useSelector } from "react-redux";

const useLogin = () => {
  const [isLogin, setIsLogin] = useState(false);
  const { user } = useSelector((state: any) => state.user);

  return { isLogin, setIsLogin, user };
};
