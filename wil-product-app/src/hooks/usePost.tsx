import axios from "axios";
import { useMutation } from "react-query";

import { postProduct } from "utils/products.utils";

const usePost = (api: string, data: any) => {
  const { mutate, isLoading, isError, isSuccess, error } = useMutation(
    () => axios.post(api, data),
    {
      onSuccess: () => {
        postProduct(api, data);
      },
    }
  );

  return { mutate, isLoading, isError, isSuccess, error };
};

export default usePost;
