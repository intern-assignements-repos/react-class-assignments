import React, { useEffect } from "react";

import useFetch from "hooks/useFetch";
import { IProduct, IUser } from "interfaces";
import TableRenderer from "./TableRenderer";
import LoaderComponent from "component/Loader";

const productApi =
  process.env.REACT_APP_PRODUCT_API || "https://fakestoreapi.com/products";
const userApi =
  process.env.REACT_APP_USER_API || "https://fakestoreapi.com/users";

const TableComponent = (props: { option: "users" | "products" }) => {
  const [selected, setSelected] = React.useState([] as number[]);
  const { option } = props;
  const api = option === "products" ? productApi : userApi;
  const { data, loading } = useFetch(api, option) as {
    data: IProduct[] | IUser[] | undefined | null;
    loading: boolean;
  };

  const [pagination, setPagination] = React.useState({
    page: 1,
    limit: 4,
  });

  const [tableLength, setTableLength] = React.useState(0);

  useEffect(() => {
    if (data) {
      setTableLength(data.length);
    }
  }, [data, pagination]);

  return (
    <>
      {loading ? (
        <LoaderComponent />
      ) : (
        <table>
          <thead>
            {option === "products" ? (
              <tr>
                <th>
                  <input
                    type="checkbox"
                    onChange={(e) => {
                      if (e.target.checked) {
                        setSelected(data?.map((item) => item.id) as number[]);
                      } else {
                        setSelected([]);
                      }
                    }}
                  />
                </th>
                <th>Title</th>
                <th>Price (in Rupees)</th>
                <th>Description</th>
              </tr>
            ) : (
              <tr>
                <th>
                  <input
                    type="checkbox"
                    onChange={(e) => {
                      if (e.target.checked) {
                        setSelected(data?.map((item) => item.id) as number[]);
                      } else {
                        setSelected([]);
                      }
                    }}
                  />
                </th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
              </tr>
            )}
          </thead>
          <tbody>
            {data &&
              TableRenderer(data, pagination, option, selected, setSelected)}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan={4}>
                <button
                  className={pagination.page === 1 ? "disabled" : ""}
                  onClick={() =>
                    setPagination({
                      ...pagination,
                      page: pagination.page - 1,
                    })
                  }
                  disabled={pagination.page === 1}
                >
                  {"<"}
                </button>

                <span>
                  {pagination.page} of{" "}
                  {Math.ceil(tableLength / pagination.limit)}
                </span>

                <button
                  className={
                    pagination.page * pagination.limit >= tableLength
                      ? "disabled"
                      : ""
                  }
                  onClick={() =>
                    setPagination({
                      ...pagination,
                      page: pagination.page + 1,
                    })
                  }
                  disabled={pagination.page * pagination.limit >= tableLength}
                >
                  {">"}
                </button>
              </td>
            </tr>
          </tfoot>
        </table>
      )}
    </>
  );
};

export default TableComponent;
