import { IProduct, IUser } from "interfaces";

const TableRenderer = (
  data: IProduct[] | IUser[],
  pagination: { page: number; limit: number },
  option: "users" | "products",
  selected: number[],
  setSelected: React.Dispatch<React.SetStateAction<number[]>>
) => {
  if (option === "users") {
    const tableData = data as IUser[];

    return tableData.map((user: IUser, index: number) => {
      if (
        index >= (pagination?.page - 1) * pagination?.limit &&
        index < pagination?.page * pagination?.limit
      ) {
        return (
          <tr key={index}>
            <td>
              <input
                type="checkbox"
                checked={selected.includes(user.id)}
                onChange={(e) => {
                  if (e.target.checked) {
                    setSelected([...selected, user.id]);
                  } else {
                    setSelected(selected.filter((id) => id !== user.id));
                  }
                }}
              />
            </td>
            <td>{user.username}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
          </tr>
        );
      }

      return null;
    });
  } else {
    const tableData = data as IProduct[];

    return tableData.map((product: IProduct, index: number) => {
      if (
        index >= (pagination?.page - 1) * pagination?.limit &&
        index < pagination?.page * pagination?.limit
      ) {
        return (
          <tr key={index}>
            <td>
              <input
                type="checkbox"
                checked={selected.includes(product.id)}
                onChange={(e) => {
                  if (e.target.checked) {
                    setSelected([...selected, product.id]);
                  } else {
                    setSelected(selected.filter((id) => id !== product.id));
                  }
                }}
              />
            </td>
            <td>{product.title.substring(0, 20)}...</td>
            <td>{product.price}</td>
            <td>{product.description.substring(0, 150)}...</td>
          </tr>
        );
      }

      return null;
    });
  }
};

export default TableRenderer;
