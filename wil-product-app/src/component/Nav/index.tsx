import React from "react";
import styled from "styled-components";

import { ITheme } from "interfaces";
import UserSvg from "assets/svgs/user.svg";
import LogoImg from "assets/images/WITS@4x.png";
import ProductSvg from "assets/svgs/product.svg";
import { useSelector } from "react-redux";

const NavBar = (props: { page: string; setPage: (page: string) => void }) => {
  const modal = useSelector((state: any) => state.modal);
  const { page, setPage } = props;

  return (
    <StyledNavBar>
      <img src={LogoImg} alt="logo" />
      <StyledNavButton
        className={page === "product" ? "" : "inactive"}
        onClick={() => setPage("product")}
      >
        <img src={ProductSvg} alt="product" />
        Product
      </StyledNavButton>
      <StyledNavButton
        className={page === "user" ? "" : "inactive"}
        onClick={() => setPage("user")}
      >
        <img src={UserSvg} alt="user" />
        User
      </StyledNavButton>
    </StyledNavBar>
  );
};

export default NavBar;

export const StyledNavBar = styled("div")`
  display: flex;
  flex-direction: column;
  min-width: 250px;
  height: 100vh;
  padding-left: 40px;
  padding-top: 20px;

  background-color: ${({ theme }: { theme: ITheme }) =>
    theme.colors.primaryLight};

  img {
    width: 124px;
    margin-bottom: 30px;
  }
`;

export const StyledNavButton = styled("div")`
  display: flex;
  align-items: center;
  justify-content: flex-start;

  width: 100%;
  height: 50px;

  padding-left: 20px;
  margin-top: 15px;

  font-weight: 600;
  font-size: 14px;
  line-height: 17px;

  background-color: ${({ theme }: { theme: ITheme }) => theme.colors.white};
  border-radius: 40px 0px 0px 40px;

  cursor: pointer;

  img {
    width: 20px;
    margin: 0;
    margin-right: 30px;
  }

  &.inactive {
    background-color: transparent;
  }
`;
