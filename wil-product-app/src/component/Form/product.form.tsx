import React from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";

import { setModal } from "store";
import { IProduct } from "interfaces";
import { StyledLoginForm } from "styles";
import ButtonComponent from "component/Button";
import { postProduct } from "utils/products.utils";

const api = "https://fakestoreapi.com/products";

const AddProductForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm();
  const dispatch = useDispatch();

  const [product, setProduct] = React.useState<IProduct | null>(null);

  const onSubmit = (data: any) => {
    const { title, price, description } = data;
    const priceNumber = Number(price) || 0;

    setProduct({
      id: Math.floor(Math.random() * 1000),
      title,
      price: priceNumber,
      description,
      category: "electronics",
      image: "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
    });

    if (product) {
      postProduct(api, product);
      dispatch(setModal(false));
      window.location.reload();
    }
  };

  const handleCancel = () => {
    reset();
    dispatch(setModal(false));
  };

  return (
    <StyledAddProductForm action="#" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <label htmlFor="name">Name</label>
        <input
          type="text"
          className="form-control"
          id="title"
          placeholder="Add Product Name"
          {...register("title", { required: true })}
        />
        {errors?.name && <p className="error">Name is required</p>}
      </div>
      <div className="form-group">
        <label htmlFor="price">Price</label>
        <input
          type="number"
          className="form-control"
          id="price"
          placeholder="Add Product Price"
          {...register("price", { required: true })}
        />
        {errors?.price && <p className="error">Price is required</p>}
      </div>
      <div className="form-group">
        <label htmlFor="description">Description</label>
        <textarea
          className="form-control"
          id="description"
          placeholder="Add Product Description"
          {...register("description", { required: true })}
        />
        {errors?.description && (
          <p className="error">Description is required</p>
        )}
      </div>
      <div className="btn-grp">
        <ButtonComponent
          btnType="secondary"
          content="Cancel"
          onClick={handleCancel}
        />
        <ButtonComponent btnType="submit" content="Add Product" />
      </div>
    </StyledAddProductForm>
  );
};

export default AddProductForm;

export const StyledAddProductForm = styled(StyledLoginForm)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  margin: 1rem 0;

  .form-group {
    border: none;

    padding: 0;

    label {
      font-weight: 400;
      font-size: 20px;
      margin-left: 10px;
      margin-bottom: 10px;
    }

    input,
    textarea {
      border: 2px solid ${({ theme }) => theme.colors.grey};
      border-radius: 1rem;
      padding: 1rem;
    }

    textarea {
      resize: none;
    }
  }

  .btn-grp {
    width: 100%;

    display: flex;
    justify-content: flex-end;

    button {
      font-weight: 500;
      font-size: 16px;

      padding: 0 auto;
      margin: 0 5px;
    }
  }
`;
