import React from "react";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

import { StyledLoginForm } from "styles";
import ButtonComponent from "component/Button";
import EyeImg from "assets/images/eye.png";
import EyeOffImg from "assets/images/eye-off.png";
import { ILoginCred } from "interfaces";
import { persistUser } from "utils/saveUser.utils";
import { useNavigate } from "react-router-dom";

const loginSchema: yup.ObjectSchema<ILoginCred> = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required(),
});

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(loginSchema),
  });

  const [showPassword, setShowPassword] = React.useState(false);
  const navigate = useNavigate();

  const onSubmit = async (data: ILoginCred | any) => {
    const currentUser = await persistUser(data);
    if (currentUser) {
      toast.success("Login Successful");
      reset();
      navigate("/dashboard");
      return;
    }

    toast.error("Invalid Credentials");
  };

  return (
    <StyledLoginForm action="#" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <input
          type="email"
          className="form-control"
          id="email"
          placeholder="Email Address"
          {...register("email", { required: true })}
        />
        {errors?.email && <p className="error">Email is required</p>}
      </div>
      <div className="form-group">
        <input
          type={showPassword ? "text" : "password"}
          className="form-control"
          id="password"
          placeholder="Password"
          {...register("password", { required: true })}
        />
        <img
          src={showPassword ? EyeOffImg : EyeImg}
          alt="eye"
          onClick={() => setShowPassword(!showPassword)}
        />
        {errors?.password && <p className="error">Password is required</p>}
      </div>
      <div className="form-group forget-password">
        <a href="#">Forgot Password?</a>
      </div>

      <ButtonComponent btnType="submit" content="Login" />
    </StyledLoginForm>
  );
};

export default LoginForm;
