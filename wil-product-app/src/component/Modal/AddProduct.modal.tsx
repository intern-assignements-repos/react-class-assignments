import React from "react";
import { createPortal } from "react-dom";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";

import { setModal } from "store";
import CloseSvg from "assets/svgs/close.svg";
import AddProductForm from "component/Form/product.form";

const Portal = ({ children }: any) => {
  return createPortal(
    children,
    document.getElementById("modal") as HTMLElement
  );
};

const AddProductModal = () => {
  const modal = useSelector((state: any) => state.modal);

  const dispatch = useDispatch();

  const handleModal = () => {
    dispatch(setModal(!modal));
  };

  return (
    <Portal>
      <StyledAddProductModalContainer
        style={{
          zIndex: modal ? 100 : -1,
        }}
      >
        <StyledAddProductModal
          style={{
            transform: modal
              ? "translate(0, 0) scale(1)"
              : "translate(110%, -30%) scale(0.2) ",
            opacity: modal ? 1 : 0,
          }}
        >
          <div>
            <h1>Add Product</h1>
            <button onClick={handleModal}>
              <img src={CloseSvg} alt="close" />
            </button>
          </div>
          <AddProductForm />
        </StyledAddProductModal>
      </StyledAddProductModalContainer>
    </Portal>
  );
};

export default AddProductModal;

export const StyledAddProductModalContainer = styled("div")`
  position: fixed;
  z-index: 100;

  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  display: flex;
  justify-content: center;
  align-items: center;

  transition: all 0.3s ease-in-out;
`;

export const StyledAddProductModal = styled("div")`
  min-width: 500px;
  /* min-height: 600px; */
  box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
  border-radius: 25px;
  background-color: #fff;

  display: flex;
  flex-direction: column;
  /* justify-content: space-between; */

  transition: all 0.3s ease-in-out;

  position: relative;
  padding: 30px 40px;

  & > div {
    display: flex;
    justify-content: space-between;
    align-items: center;

    margin-bottom: 30px;

    h1 {
      font-weight: 600;
      font-size: 24px;
    }

    button {
      width: 20px;
      height: 20px;
      display: flex;
      justify-content: center;
      align-items: center;
      border: none;
      border-radius: 50%;
      background-color: transparent;
      color: #fff;
      cursor: pointer;
      cursor: pointer;

      img {
        width: 100%;
        color: #fff;
      }
    }
  }
`;
