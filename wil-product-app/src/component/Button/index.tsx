import React from "react";
import styled from "styled-components";
import { StyledButton, StyledPrimaryButton } from "styles";

const ButtonComponent = ({
  btnType,
  content,
  onClick,
}: {
  btnType: string;
  content: string;
  onClick?: () => void;
}) => {
  if (btnType === "primary")
    return (
      <StyledPrimaryButton onClick={onClick}>{content}</StyledPrimaryButton>
    );

  if (btnType === "secondary")
    return (
      <StyledSecondaryComponent onClick={onClick}>
        {content}
      </StyledSecondaryComponent>
    );

  if (btnType === "submit")
    return <StyledButton type="submit">{content}</StyledButton>;

  return <StyledButton>{content}</StyledButton>;
};

export default ButtonComponent;

export const StyledSecondaryComponent = styled(StyledButton)`
  background-color: transparent;
  background: none;
  color: ${({ theme }) => theme.colors.secondary};
`;
