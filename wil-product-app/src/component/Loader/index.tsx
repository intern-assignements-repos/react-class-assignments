import React from "react";
import styled from "styled-components";

const LoaderComponent = () => {
  return (
    <StyledLoader>
      <div />
    </StyledLoader>
  );
};

export default LoaderComponent;

export const StyledLoader = styled("div")`
  height: 100%;

  & > div {
    width: 75px;
    height: 75px;
    border-radius: 50%;

    &::after {
      content: "";
      display: block;
      width: 75px;
      height: 75px;
      border-radius: 50%;
      border: 10px solid ${({ theme }) => theme.colors.primary};
      border-color: ${({ theme }) => theme.colors.primary} transparent
        ${({ theme }) => theme.colors.secondary} transparent;
      animation: loader 1.2s linear infinite;
    }

    @keyframes loader {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
  }
`;
