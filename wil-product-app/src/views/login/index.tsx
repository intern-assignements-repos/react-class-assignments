import React from "react";

import LoginForm from "component/Form/login.form";
import {
  StyledBGCircles,
  StyledBGSquare,
  StyledLoginPageContent,
  LoginFormContainer,
  ImageWrapper,
  LoginFormHeader,
} from "styles";
import WilLogo from "assets/images/WITS@4x.png";

const LoginPageViews = () => {
  return (
    <>
      <StyledBGCircles>
        <div />
        <div />
      </StyledBGCircles>
      <StyledBGSquare>
        <div />
        <div />
      </StyledBGSquare>
      <StyledLoginPageContent>
        <LoginFormContainer>
          <ImageWrapper>
            <img src={WilLogo} alt="wil-logo" />
          </ImageWrapper>
          <LoginFormHeader>
            <h1>Login with WIL </h1>
            <p>
              New User? <a href="#">Create an Account</a>
            </p>
          </LoginFormHeader>
          <LoginForm />
        </LoginFormContainer>
      </StyledLoginPageContent>
    </>
  );
};

export default LoginPageViews;
