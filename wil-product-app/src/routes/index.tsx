import React from "react";
import { Routes as ReactRoutes, Route } from "react-router-dom";

import { IRoute } from "interfaces";
import LoginPage from "pages/login";
import PublicRoutes from "./public.routes";
import DashboardPage from "pages/dashboard";
import PrivateRoutes from "./private.routes";
import UserPage from "pages/dashboard/user.page";
import ProductPage from "pages/dashboard/product.page";

const DashboardRoutes: IRoute[] = [
  {
    path: "product",
    component: <ProductPage />,
    restricted: true,
  },
  {
    path: "user",
    component: <UserPage />,
    restricted: true,
  },
];

const rootRoutes: IRoute[] = [
  {
    path: "/",
    component: <LoginPage />,
    restricted: false,
  },
  {
    path: "dashboard",
    component: <DashboardPage />,
    restricted: true,
    children: DashboardRoutes,
  },
];

const RouteComponent = () => {
  return (
    <ReactRoutes>
      {rootRoutes.map((route: IRoute, index: number) => {
        const { path, component, restricted, children } = route;

        return (
          <Route
            key={index}
            path={path}
            element={
              restricted ? (
                <PrivateRoutes component={component} />
              ) : (
                <PublicRoutes component={component} />
              )
            }
          />
        );
      })}
    </ReactRoutes>
  );
};

export default RouteComponent;
