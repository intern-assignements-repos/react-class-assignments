import React from "react";
import { Navigate } from "react-router-dom";
import { IRouteComponentProps } from "../interfaces";
import isLogin from "../utils/users.utils";

const PublicRoutes = ({ component }: IRouteComponentProps) =>
  isLogin() ? <Navigate to="/dashboard" /> : component;

export default PublicRoutes;
