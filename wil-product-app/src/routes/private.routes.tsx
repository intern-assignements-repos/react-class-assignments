import React from "react";
import { Navigate } from "react-router-dom";
import { IRouteComponentProps } from "../interfaces";
import isLogin from "../utils/users.utils";

const PrivateRoutes = ({ component }: IRouteComponentProps) =>
  !isLogin() ? <Navigate to="/" /> : component;

export default PrivateRoutes;
