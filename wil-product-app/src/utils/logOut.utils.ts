import isLogin from "./users.utils";

export const logOut = () => {
  if (isLogin()) localStorage.removeItem("wpaCurrentUser");

  window.location.href = "/";

  return null;
};
