import { IUser } from "interfaces";

const isLogin = (): boolean => localStorage.getItem("wpaCurrentUser") !== null;

export default isLogin;

export const getCurrentUser = (): IUser | null => {
  const user = localStorage.getItem("wpaCurrentUser");
  return user ? JSON.parse(user) : null;
};

export const getAllUsers = (): IUser[] | null => {
  const users = localStorage.getItem("wpaUsers");
  return users ? JSON.parse(users) : null;
};

export const isUsers = (): boolean => {
  const users = localStorage.getItem("wpaUsers");
  return users ? true : false;
};

export const setCurrentUser = (user: IUser) => {
  localStorage.setItem("wpaCurrentUser", JSON.stringify(user));
};

export const setUsers = (users: IUser[]) => {
  localStorage.setItem("wpaUsers", JSON.stringify(users));
};

export const removeCurrentUser = () =>
  isLogin() && localStorage.removeItem("wpaCurrentUser");

export const removeUsers = () =>
  isUsers() && localStorage.removeItem("wpaUsers");
