import { IProduct } from "interfaces";
import axios from "axios";

export const isProducts = (): boolean => {
  const porducts = localStorage.getItem("wpaProducts");
  return porducts ? true : false;
};

export const getProducts = (): IProduct[] | null => {
  const products = localStorage.getItem("wpaProducts");
  return products ? JSON.parse(products) : null;
};

export const setProducts = (products: IProduct[]) => {
  localStorage.setItem("wpaProducts", JSON.stringify(products));
};

export const removeProducts = () =>
  isProducts() && localStorage.removeItem("wpaProducts");

export const postProduct = async (api: string, product: IProduct) => {
  try {
    const products = getProducts() as IProduct[];
    if (!products) throw new Error("No products in local storage");
    setProducts([product, ...products]);

    const { title, price, description } = product;
    const res = await axios.post(
      api,
      {
        title,
        price,
        description,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    console.log("Response :", res.data);

    if (res.data.error) throw new Error(res.data.error);
    // removeProducts();
    // return res.data;
    console.log("Product added to local storage");
  } catch (err) {
    console.log(err);
  }
};
