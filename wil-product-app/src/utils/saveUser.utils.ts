import { ILoginCred, IUser } from "interfaces";
import { userFetch } from "services/dataFetch";

const api = "https://fakestoreapi.com/users";

export const persistUser = async (user: ILoginCred) => {
  try {
    let userExists: IUser | undefined;
    const wpaUsers: IUser[] | null = JSON.parse(
      localStorage.getItem("wpaUsers") || "null"
    );

    if (wpaUsers) {
      userExists = wpaUsers.find(
        (u: IUser) => u.email === user.email && u.password === user.password
      );

      if (userExists) {
        localStorage.setItem("wpaCurrentUser", JSON.stringify(userExists));
      }
    } else {
      const data: IUser[] | undefined = await userFetch(api);

      if (data) {
        localStorage.setItem("wpaUsers", JSON.stringify(data));
        userExists = data.find((u: IUser) => u.email === user.email);

        if (userExists) {
          localStorage.setItem("wpaCurrentUser", JSON.stringify(userExists));
        }
      }
    }

    if (!userExists) {
      throw new Error("User does not exist");
    }

    return userExists;
  } catch (error) {
    console.log(error);
    return null;
  }
};
