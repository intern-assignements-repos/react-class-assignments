import { ITheme } from "../../interfaces";

const theme: ITheme = {
  colors: {
    primary: "#FACB30",
    primaryLight: "#FFEBB1",
    secondary: "#1D2E88",

    background: "#F5F5F5",
    backgroundDark: "#FAFAFA",
    backgroundLight: "#FEFEFE",

    white: "#FFFFFF",
    black: "#000000",
    blackLight: "#272727",
    grey: "#F5F6F9",
    fade: "rgba(39, 39, 39, 0.4)",
  },
  fonts: {
    inter: `'Inter', sans-serif;`,
    poppins: `'Poppins', sans-serif;`,
  },
};

export default theme;
