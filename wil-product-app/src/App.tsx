import React from "react";
import styled, { ThemeProvider } from "styled-components";
import { ToastContainer } from "react-toastify";

import RouteComponent from "routes";
import theme from "constants/theme";
import { GlobalStyles } from "styles";
import { useSelector } from "react-redux";

const App = () => {
  const modal = useSelector((state: any) => state.modal);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <AppContainer modal={modal}>
        <RouteComponent />
      </AppContainer>
      <ToastContainer position="top-center" />
    </ThemeProvider>
  );
};

export default App;

const AppContainer = styled("div")<{ modal: boolean }>`
  filter: ${({ modal }) => (modal ? "blur(5px)" : "none")};
  transition: filter 0.3s ease-in-out;
  z-index: 1;
`;
