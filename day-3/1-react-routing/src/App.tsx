import { Link } from "react-router-dom";

import Router from "routes";

import { AppContainer, AppHeader, AppImage } from "styles/App.styles";
import logo from "./logo.svg";

const App = () => {
  return (
    <AppContainer>
      <AppHeader>
        <AppImage src={logo} className="App-logo" alt="logo" />
        <p>Example of React Routing</p>
        <div>
          <Link to="/" className="App-link">
            Home
          </Link>
          <Link to="/dashboard" className="App-link">
            Dashboard
          </Link>
          <Link to="/about" className="App-link">
            About
          </Link>
          <Link to="/contact" className="App-link">
            Contact
          </Link>
          <Link to="*" className="App-link">
            Not Found
          </Link>
        </div>
        <Router />
      </AppHeader>
    </AppContainer>
  );
};

export default App;
