import { StyledButton } from "components/Button";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const navigate = useNavigate();
  const [data, setData] = useState<string>("");

  const handleSubmit = () => {
    if (data.length !== 0) navigate("/about", { state: { data: data } });
    else alert("Data Can't be empty!");
  };

  return (
    <>
      <p>Write for About Page Section</p>
      <textarea rows={8} cols={50} onChange={(e) => setData(e.target.value)} />
      <StyledButton type="submit" onClick={handleSubmit}>
        Update About
      </StyledButton>
    </>
  );
};

export default Dashboard;
