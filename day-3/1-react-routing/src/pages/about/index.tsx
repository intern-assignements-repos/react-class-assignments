import React from "react";
import { useLocation } from "react-router-dom";

const AboutPage = () => {
  const location = useLocation();

  return (
    <>
      <h1>AboutPage</h1>
      <p>
        {location.state?.data ? (
          <>{location.state.data}</>
        ) : (
          `Nothing to show here, update it from Dashboard`
        )}
      </p>
    </>
  );
};

export default AboutPage;
