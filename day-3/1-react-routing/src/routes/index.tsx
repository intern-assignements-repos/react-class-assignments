import ContactPage from "pages/contact";
import Dashboard from "pages/dashboard";
import PageNotFound from "pages/notFound";
import React from "react";
import { Routes, Route } from "react-router-dom";
import AboutPage from "../pages/about";
import HomePage from "../pages/home";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<HomePage />}/>
      <Route path="/about" element={<AboutPage />}/>
      <Route path="/contact" element={<ContactPage />}/>
      <Route path="/dashboard" element={<Dashboard />}/>
      <Route path="*" element={<PageNotFound />} />
    </Routes>
  );
};

export default Router;
