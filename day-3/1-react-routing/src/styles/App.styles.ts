import styled from "styled-components";

export const AppContainer = styled("div")`
    text-align: center;
`;

export const AppHeader = styled("header")`
    background-color: #282c34;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    font-size: calc(10px + 2vmin);
    color: white;

    .App-link {
        color: #61dafb;
        margin: 1rem;
    }
`;

export const AppImage = styled("img")`
    height: 10vmin;
    margin-top: 5vmin;
    pointer-events: none;

    @media (prefers-reduced-motion: no-preference) {
        & {
            animation: App-logo-spin infinite 20s linear;
        }
    }

    @keyframes App-logo-spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

`;