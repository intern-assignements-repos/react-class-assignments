import styled from "styled-components";

export const StyledButton = styled("button")`
    border: none;
    font-size: 1.2rem;
    margin: 1rem;
    padding: 1rem 4rem;
    color: #282c34;
    background-color: #61dafb;
    cursor: pointer;
`;